<?php namespace Daniel\Events\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateDanielEventsEvents extends Migration
{
    public function up()
    {
        Schema::create('daniel_events_events', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('tittle', 50);
            $table->string('slug', 75);
            $table->string('description', 150);
            $table->text('content');
            $table->dateTime('date');
            $table->string('location', 100);
            $table->string('picture', 100);
            $table->integer('category_id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->dateTime('update_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('daniel_events_events');
    }
}
