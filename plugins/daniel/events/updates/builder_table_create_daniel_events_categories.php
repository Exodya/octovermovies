<?php namespace Daniel\Events\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateDanielEventsCategories extends Migration
{
    public function up()
    {
        Schema::create('daniel_events_categories', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('tittle', 50);
            $table->string('description', 150);
            $table->string('slug', 100);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('daniel_events_categories');
    }
}
