<?php namespace Daniel\Galeries\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateDanielGaleriesPictures extends Migration
{
    public function up()
    {
        Schema::table('daniel_galeries_pictures', function($table)
        {
            $table->text('items')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('daniel_galeries_pictures', function($table)
        {
            $table->dropColumn('items');
        });
    }
}
