<?php namespace Daniel\Events\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateDanielEventsEvents3 extends Migration
{
    public function up()
    {
        Schema::table('daniel_events_events', function($table)
        {
            $table->dropColumn('picture');
        });
    }
    
    public function down()
    {
        Schema::table('daniel_events_events', function($table)
        {
            $table->string('picture', 100)->nullable();
        });
    }
}
