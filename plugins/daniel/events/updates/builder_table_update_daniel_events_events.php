<?php namespace Daniel\Events\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateDanielEventsEvents extends Migration
{
    public function up()
    {
        Schema::table('daniel_events_events', function($table)
        {
            $table->renameColumn('update_at', 'updated_at');
        });
    }
    
    public function down()
    {
        Schema::table('daniel_events_events', function($table)
        {
            $table->renameColumn('updated_at', 'update_at');
        });
    }
}
