<?php namespace Daniel\Galeries\Models;

use Model;

/**
 * Model
 */
class Galery extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'daniel_galeries_pictures';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    protected $jsonable = [
        'items'
    ];
}
