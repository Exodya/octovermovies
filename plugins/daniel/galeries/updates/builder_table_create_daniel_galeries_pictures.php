<?php namespace Daniel\Galeries\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateDanielGaleriesPictures extends Migration
{
    public function up()
    {
        Schema::create('daniel_galeries_pictures', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 100);
            $table->string('code', 50);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('daniel_galeries_pictures');
    }
}
